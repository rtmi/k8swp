#!/usr/bin/env bash

# IMPORTANT - .gitignore must exclude private files before this job


# To debug cron, enable to display env fields and repeat commands:
##set
##set -x

export WP_CLI_CONFIG_PATH="/var/www/html"
export PATH=$PATH:/usr/local/bin
# /var/www/save is a persistent volume mount for the pod
cd /var/www/save
git fetch -q --all
git reset --hard origin/develop

# dump db
cd /var/www/html
rm -f /var/www/save/dbdump/.mysqldump.sql
mkdir -p /var/www/save/dbdump
##/usr/bin/mysqldump -h mysql -u root  --all-databases > /var/www/save/dbdump/.mysqldump.sql  ## -R --triggers
wp db export --add-drop-table --allow-root /var/www/save/dbdump/.mysqldump.sql
chown -R www-data:www-data /var/www/save/dbdump
cd /var/www/save

# instead of changing /var/www/html, copy to heroku-wp dir layout
rsync -q -a /var/www/html/wp-content/themes/ /var/www/save/public/wp-content/themes
rsync -q -a /var/www/html/wp-content/plugins/ /var/www/save/public/wp-content/plugins

# Save the /var/www/save/public Wordpress files
/usr/bin/git checkout develop &> /dev/null
/usr/bin/git add public dbdump
/usr/bin/git commit -q -m "Sync files" 
/usr/bin/git push -q -u origin develop &> /dev/null


