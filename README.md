# What is this

This uses [wordpress](https://registry.hub.docker.com/_/wordpress/) as the base image to run inside a Kubernetes pod. For VCS, a cron job mirrors /var/www/html/wp-content/plugins | themes to Github.
 Also, msqldump is saved to /var/www/save/dbdump

![diagram](diagram/diagram.svg)

