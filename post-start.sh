#!/usr/bin/env bash

# /var/www/save is a persistent volume mount for the pod
cd /var/www/save
git init --quiet
git remote add origin $WP_VCS_URL 
##git fetch -q --all
##git reset --hard origin/develop
git config user.email "mirror@$MY_POD_IP"
git config user.name "$MY_POD_NAME"

# Exclude private files from version control
cat /opt/Wordpress.gitignore >> /var/www/save/.gitignore

# To debug cron, enable stdout capture:
##echo "*/10 * * * * /root/cronjob-mirror.sh >>/var/log/cronrun 2>&1" >/root/crontmp
echo "30 * * * * /root/cronjob-mirror.sh" >/root/crontmp
crontab /root/crontmp
rm /root/crontmp

# Skip Mysql password prompt:
echo "[client]
 password=$WORDPRESS_DB_PASSWORD" > /root/.my.cnf
mkdir -p /var/www/save/dbdump


#TODO manual steps gen+add key to github:
##ssh-keygen -t rsa -b 4096 "mirror@$MY_POD_IP"

# Enable jobs
##service cron start

